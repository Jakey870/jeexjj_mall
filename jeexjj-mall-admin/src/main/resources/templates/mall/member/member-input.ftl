<#--
/****************************************************
 * Description: 用户表的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/member/save" id=tabId>
   <input type="hidden" name="id" value="${member.id}"/>
   
   <@formgroup title='用户名'>
	<input type="text" name="username" value="${member.username}" check-type="required">
   </@formgroup>
   <@formgroup title='密码，加密存储'>
	<input type="text" name="password" value="${member.password}" check-type="required">
   </@formgroup>
   <@formgroup title='注册手机号'>
	<input type="text" name="phone" value="${member.phone}" >
   </@formgroup>
   <@formgroup title='注册邮箱'>
	<input type="text" name="email" value="${member.email}" >
   </@formgroup>
   <@formgroup title='created'>
	<@date name="created" dateValue=member.created required="required" default=true/>
   </@formgroup>
   <@formgroup title='updated'>
	<@date name="updated" dateValue=member.updated required="required" default=true/>
   </@formgroup>
   <@formgroup title='sex'>
	<input type="text" name="sex" value="${member.sex}" >
   </@formgroup>
   <@formgroup title='address'>
	<input type="text" name="address" value="${member.address}" >
   </@formgroup>
   <@formgroup title='state'>
	<input type="text" name="state" value="${member.state}" check-type="number">
   </@formgroup>
   <@formgroup title='头像'>
	<input type="text" name="file" value="${member.file}" >
   </@formgroup>
   <@formgroup title='description'>
	<input type="text" name="description" value="${member.description}" >
   </@formgroup>
   <@formgroup title='积分'>
	<input type="text" name="points" value="${member.points}" check-type="number">
   </@formgroup>
   <@formgroup title='余额'>
	<input type="text" name="balance" value="${member.balance}" >
   </@formgroup>
</@input>