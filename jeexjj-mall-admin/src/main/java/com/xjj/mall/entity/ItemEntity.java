/****************************************************
 * Description: Entity for 商品表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import java.math.BigDecimal;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;

public class ItemEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public ItemEntity(){}
    private String title;//商品标题
    private String sellPoint;//商品卖点
    private BigDecimal price;//商品价格
    private Integer num;//库存数量
    private Integer limitNum;//售卖数量限制
    private String image;//商品图片
    private Long cid;//所属分类
    private Integer status;//商品状态 1正常 0下架
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date created;//创建时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updated;//更新时间
    /**
     * 返回商品标题
     * @return 商品标题
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * 设置商品标题
     * @param title 商品标题
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * 返回商品卖点
     * @return 商品卖点
     */
    public String getSellPoint() {
        return sellPoint;
    }
    
    /**
     * 设置商品卖点
     * @param sellPoint 商品卖点
     */
    public void setSellPoint(String sellPoint) {
        this.sellPoint = sellPoint;
    }
    
    /**
     * 返回商品价格
     * @return 商品价格
     */
    public BigDecimal getPrice() {
        return price;
    }
    
    /**
     * 设置商品价格
     * @param price 商品价格
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    /**
     * 返回库存数量
     * @return 库存数量
     */
    public Integer getNum() {
        return num;
    }
    
    /**
     * 设置库存数量
     * @param num 库存数量
     */
    public void setNum(Integer num) {
        this.num = num;
    }
    
    /**
     * 返回售卖数量限制
     * @return 售卖数量限制
     */
    public Integer getLimitNum() {
        return limitNum;
    }
    
    /**
     * 设置售卖数量限制
     * @param limitNum 售卖数量限制
     */
    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }
    
    /**
     * 返回商品图片
     * @return 商品图片
     */
    public String getImage() {
        return image;
    }
    
    /**
     * 设置商品图片
     * @param image 商品图片
     */
    public void setImage(String image) {
        this.image = image;
    }
    
    /**
     * 返回所属分类
     * @return 所属分类
     */
    public Long getCid() {
        return cid;
    }
    
    /**
     * 设置所属分类
     * @param cid 所属分类
     */
    public void setCid(Long cid) {
        this.cid = cid;
    }
    
    /**
     * 返回商品状态 1正常 0下架
     * @return 商品状态 1正常 0下架
     */
    public Integer getStatus() {
        return status;
    }
    
    /**
     * 设置商品状态 1正常 0下架
     * @param status 商品状态 1正常 0下架
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    /**
     * 返回创建时间
     * @return 创建时间
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置创建时间
     * @param created 创建时间
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回更新时间
     * @return 更新时间
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置更新时间
     * @param updated 更新时间
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.ItemEntity").append("ID="+this.getId()).toString();
    }
}

